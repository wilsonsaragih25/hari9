<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

// Instance class Animal
$sheep = new Animal("Shaun");

// Tampilkan informasi hewan baru
echo "name: " . $sheep->name . "<br>";
echo "legs: " . $sheep->legs . "<br>";
echo "cold blooded: " . $sheep->cold_blooded . "<br>";

// Instance class Frog
$kodok = new Frog("Buduk");
echo "<br>name: " . $kodok->name . "<br>";
echo "legs: " . $kodok->legs . "<br>";
echo "cold blooded: " . $kodok->cold_blooded . "<br>";
$kodok->jump(); // "jump: hop hop"

// Instance class Ape
$sungokong = new Ape("Kera Sakti");
echo "<br>name: " . $sungokong->name . "<br>";
echo "legs: " . $sungokong->legs . "<br>";
echo "cold blooded: " . $sungokong->cold_blooded . "<br>";
$sungokong->yell(); // "yell: Auooo"

?>
